#!/bin/bash

# This script prints the number of days in the current month, and gives
# information about leap year if the current month is February.

currentMonth=`date +"%m" | sed 's/^0//' -`
currentMonthName=$(date +"%B")
currentYear=$(date +"%Y")

if [ $currentMonth -le 7 ]; then
	if [ $(($currentMonth % 2)) -ne 0 ]; then
		echo "Current month is $currentMonthName and has 31 days"
	elif [ $currentMonth -ne 2 ]; then
		echo "Current month is $currentMonthName and has 30 days"
	else
		if (( ($currentYear % 400) == "0" )) || \
			(( ($currentYear % 4) == "0" && \
			($currentYear % 100) != "0" )); then
			echo "Current month is $currentMonthName and year"\
				"$currentYear is leap, so it has 29 days"
		else
			echo "Current month is $currentMonthName and has 28 days"
		fi
	fi
else
	if [ $(($currentMonth % 2)) -eq 0 ]; then
		echo "Current month is $currentMonthName and has 31 days"
	else
		echo "Current month is $currentMonthName and has 30 days"
	fi
fi
