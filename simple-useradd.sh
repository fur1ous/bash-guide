#!/bin/bash

if [ $# -ne 1 ]; then
	echo "Only one argument needed! USAGE: $0 <username>"
	exit 1
fi


echo "Checking /etc/passwd for free user ID..."
lastId=$(awk 'BEGIN { FS=":" } { print $3 }' /etc/passwd | sort -nr | head -1)
availableId=$(( $lastId + 1 ))
echo "Choosing user ID: $availableId for user creation!"


echo "Creating user's private group..."

lastGroupId=$(awk 'BEGIN { FS=":" } { print $3 }' /etc/group | sort -nr | head -1)
availableGroupId=$(( $lastGroupId + 1 ))
groupadd -g $availableGroupId $1
echo "Group with ID: $groupId created!"

read -p "Enter description for user $1: " userDescription
echo "Choose user shell: "
cat << BEGIN
sh
ksh
tcsh
csh
bash
BEGIN
read -p "?" userShell
$userShell != "sh" || $userShell != "ksh" || $userShell != "tcsh" || $userShell != "csh" || $userShell != bash && (echo "Supply name of valid shell!"; exit 1)
userShell="/bin/$userShell"

read -p "Enter user expiration date in format (YYYY-MM-DD): " expDate
read -p "Enter user supplementary groups: " suppGroups

echo "$1:x:$availableId:$groupId:$userDescription:/home/$1:$userShell" >> /etc/passwd
echo "$1:x:$groupId:" >> /etc/groups

# Create home with correct permissions
mkdir /home/"$1"
chown -R "$1":"$1" /home/"$1"
chmod 700 -R /home/"$1"

awk '{ print $1 }' <(printf "%s\n" $suppGroups) | sed -i "s/$/$1/" /etc/group

chage -E $expDate $1

passwd $1 << PASSWORD
ExamplePasswd!
PASSWORD

echo "User $1 created!"
