#!/bin/bash

# This script prints the number of days in the current month, and gives
# information about leap year if the current month is February.

currentMonth=`date +"%m"`
currentMonthName=$(date +"%B")
currentYear=$(date +"%Y")

case $currentMonth in
	01)
		echo "Current month is $currentMonthName and has 31 days"
		;;
	02)
		if (( ($currentYear % 400) == "0" )) || \
			(( ($currentYear % 4) == "0" && \
			($currentYear % 100) != "0" )); then
			echo "Current month is $currentMonthName and year"\
				"$currentYear is leap, so it has 29 days"
		else
			echo "Current month is $currentMonthName and has 28 days"
		fi
		;;
	03)
		echo "Current month is $currentMonthName and has 31 days"
		;;
	04)
		echo "Current month is $currentMonthName and has 30 days"
		;;
	05)
		echo "Current month is $currentMonthName and has 31 days"
		;;
	06)
		echo "Current month is $currentMonthName and has 30 days"
		;;
	07)
		echo "Current month is $currentMonthName and has 31 days"
		;;
	08)
		echo "Current month is $currentMonthName and has 31 days"
		;;
	09)
		echo "Current month is $currentMonthName and has 30 days"
		;;
	10)
		echo "Current month is $currentMonthName and has 31 days"
		;;
	11)
		echo "Current month is $currentMonthName and has 30 days"
		;;
	12)
		echo "Current month is $currentMonthName and has 31 days"
		;;
esac
