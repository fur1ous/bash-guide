#!/bin/bash

# This script will test if we're in a leap year or not.

echo "Type the year that you want to check (4 digits), followed by [ENTER]:"
read year

if [ $[$year % 400] -eq 0 ]; then
	echo "This is a leap year. February has 29 days."
elif [ $[$year % 4 ] -eq 0 ]; then
	if [ $[$year % 100] -ne 0 ]; then
		echo "THis is a leap year. February has 29 days."
	else
		echo "This is not a leap year. February has 28 days."
	fi
else
	echo "This is not a leap year. February has 28 days."
fi
