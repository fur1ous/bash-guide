#!/bin/bash

# Script that writes image to null device.

trap "echo This action will make the written image unusable!" SIGINT
dd if=~/sample.img of=/dev/null		# This does not do any useful thing,
					# it is just for presentation of traps.
