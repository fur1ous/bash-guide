#!/bin/bash

cd `pwd`
if [ ! -d histdir ]; then
	mkdir histdir
fi
i=0
while true; do
	echo -n "Enter URL, or 'b'-back, or 'q'-quit: "
	read choice
	case "$choice" in
		q)
			echo "Exiting browser..."
			break 2
			;;
		b)	set -x	
			if [ $i -le 1 ]; then
				:
			else
				for ((h=${i}-2 ; h>=0 ; h-- )); do
					# There is a bug when exhausting history
					# and going forward and again going back
					histpage=$(ls -C1 histdir/* | grep ${h}\.*)
					links -dump "$histpage"
					rm -f "$histpage"
					((i--))
					break
				done		
			fi
			set -x
			;;
		*)
			links -dump $choice
			if [ "$i" -lt "9" ]; then
				wget -O histdir/${i}.${choice##*://} "$choice"
				hist[i]=$choice
				((i++))
			else
				for h in "${hist[@]}"; do
					shift
					break
				done
				wget -O histdir/${i}.${choice##*://}
				hist[9]=$choice
			fi
			;;
	esac
done
rm -f histdir/*
