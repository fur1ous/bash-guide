#!/bin/bash

echo "*** TOP USERS BY DISK SPACE USAGE ***"
for user in $(ls /home | awk '{ print $1 }'); do
	find /home/"$user"/* -user "$user" -print0 2>/dev/null | du -ch --files0-from=- |
	       awk -v user="$user" '/\<total\>/ { print user ": " $1 }'
done | sort -rh -k2 | head -n 3 | mail -s "Disk space usage" root@localhost
