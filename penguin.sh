#!/bin/bash

# This script lets you present different menus to Tux. He will only be happy
# when given a fish. We've also added a dolphin and (presumably) a camel.

if [ "$menu" == fish ]; then
	if [ "$animal" == "penguin" ]; then
		echo -e "Hmmmmm fish... Tux happy!\n"
	elif [ "$animal" == "dolphin" ]; then
		echo -e "\a\a\aPweetpeettreetppeterdepweet!\a\a\a\n"
	else
		echo -e "*prrrrrrrt*\n"
	fi
else
	if [ "$animal" == "penguin" ]; then
		echo -e "Tux don't like that. Tux wants fish!\n"
		exit 1
	elif [ "$animal" == "dolphin" ]; then
		echo -e "\a\a\a\a\a\aPweetpeettreetppeterdepweet!\a\a\a"
		exit 2
	else
		echo -e "Will you read this sign?!\n"
		exit 3
	fi
fi
