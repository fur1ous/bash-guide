#!/bin/bash

intlist=("Apache" "SSH" "NTP" "Other")

select server in "${intlist[@]}"; do
	case "$server" in
		"Apache")
			 echo "Server name: $(ssh root@server1 hostname)"
			 ;;
		 "SSH")
			 echo "Server SSH status: $(ssh root@server2 systemctl status sshd)"
			 ;;
		 "NTP")
			 echo "Server ntp info: $(ssh root@server3 chronyc sourcestats)"
			 ;;
		 "Other")
			 read -p "Enter the server name to check: " server
			 read -p "Enter the name of process to check: " process
			 echo "Checking $process status..."
			 ssh root@$server systemctl status $process
			 echo "Done."
			 ;;
		 *)
			 echo "Not a valid input, choose again!"
	 esac
done
