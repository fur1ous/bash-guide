#!/bin/bash

dir="$PWD"

if [ "$#" -ne 1 ]; then
	echo "USAGE: ${0##*/} directory-name"
	exit 1
elif [ ! -d "$dir/$1" ]; then
	echo "Directory does not exist!"
	exit 2
fi


biggestFiles=$(ls -lS "$dir/$1/" | grep -v total | head -n5 | awk '{ print $NF }')
mostRecent=$(ls -lt "$dir/$1/" | grep -v total | head -n5 | awk '{ print $NF }')

echo -e "Biggest files:\n$biggestFiles"
echo
echo -e "Most recent files:\n$mostRecent"
